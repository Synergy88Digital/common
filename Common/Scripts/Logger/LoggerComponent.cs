using UnityEngine;
using System.Collections;

using Common.Logger;

/**
 * A component that can be attached to a GameObject so it could run Logger updates.
 */
public class LoggerComponent : MonoBehaviour {
	
	[SerializeField]
	private string name;
	
	private Logger logger;
	
	void Awake() {
		logger = Logger.GetInstance();
		logger.SetName(name);
	}
	
	// Update is called once per frame
	void Update() {
		logger.Update();
	}

}
